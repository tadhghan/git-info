# Creating a new repo from scratch

Create the repo on bitbucket.
Then run these commands on your command line:
git clone https://tadhghan@bitbucket.org/path-to/your-repo.git
cd git-info
echo "# My project's README" >> README.md
git add README.md
git commit -m "Initial commit"
git push -u origin master  


# Add unversioned code to a repository

First create the repo on bitbucket.

From your terminal, change to the root directory of your existing code.
Initialize the directory under source control using the following command: git init
Add the existing files to the repository you have initialized: git add .
Commit the files: git commit -m 'initial commit of full repository'
From the Overview page of the repository you created in Bitbucket,
choose I have an existing project.

Connect your new local repository to the remote repository on Bitbucket.
To do that, copy the git remote path that you see under 'I have an existing project'
and enter it in the command line.
Step 1: Switch to your repository's directory: cd /path/to/your/repo
Step 2: Connect your existing repository to Bitbucket: git remote add origin https://tadhghan@bitbucket.org/tadhghan/git-info.git
git push -u origin master

Push all the code in your local repo to Bitbucket with the following command: git push -u origin --all

#
